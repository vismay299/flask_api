#!/bin/python3
import unittest
from app import suma

class Test(unittest.TestCase):
     
     def test_sum(self):
         val = suma(4, 5)
         self.assertEqual(val,9)
if __name__ == "__main__":
     unittest.main()
